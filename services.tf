resource "aws_ecs_service" "tfmEcs" {
  name            = "tfmEcs"
  cluster         = aws_ecs_cluster.tfm-cluster.id
  task_definition = var.task_definition_arn
  desired_count   = 1
#   iam_role        = aws_iam_role.ecs_service_role.arn
  depends_on      = [aws_iam_role_policy.ecs_service_role_policy]

  network_configuration {
    subnets = [aws_subnet.tfm-public.id]
    security_groups = [aws_security_group.tfm-sg.id]
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.tfm-tg.arn
    container_name   = "ecsNode"
    container_port   = 5000
  }
}


resource "aws_lb_target_group" "tfm-tg" {
  depends_on = [
    aws_lb.tfm-lb
  ]
  name     = "tf-lb-tg"
  port     = 5000
  protocol = "TCP"
  target_type = "ip"
  vpc_id   = aws_vpc.tfm-vpc.id
}

resource "aws_lb_listener" "tfm-tg-ls" {
  load_balancer_arn = aws_lb.tfm-lb.arn
  port              = "5000"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.tfm-tg.arn
  }
}