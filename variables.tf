variable "amis" {
    description = "Which AMI to spawn. Defaults to the AWS ECS optimized images."
    default = {
        us-east-2 = "ami-0754ccaae435c9466"
    }
}

variable "region" {
    description = "The AWS region to create resources in."
    default = "us-east-2"
}

variable "instance_type" {
    default = "m5zn.large"
}

variable "ecs_cluster_name" {
    description = "The name of the Amazon ECS cluster."
    default = "tfm-cluster"
}

variable "task_definition_arn" {
    description = "task def arn"
    default = "arn:aws:ecs:us-east-2:288274762440:task-definition/ecsNode:1"
}

variable "executioner_arn" {
    default = "arn:aws:iam::288274762440:role/ecsTaskExecutionRole"
}
