provider "aws" {
  region     = "us-east-2"
  access_key = var.AWS_KEY
  secret_key = var.AWS_SECRET
}


resource "aws_vpc" "tfm-vpc" {
  cidr_block       = "172.2.0.0/24"
  instance_tenancy = "default"

  tags = {
    Name = "terraform vpc"
  }
}

resource "aws_key_pair" "tfm-key" {
  key_name   = "tfm-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD3F6tyPEFEzV0LX3X8BsXdMsQz1x2cEikKDEY0aIj41qgxMCP/iteneqXSIFZBp5vizPvaoIR3Um9xK7PGoW8giupGn+EPuxIA4cDM4vzOqOkiMPhz5XK0whEjkVzTo4+S0puvDZuwIsdiW9mxhJc7tgBNL0cYlWSYVkz4G/fslNfRPW5mYAM49f4fhtxPb5ok4Q2Lg9dPKVHO/Bgeu5woMc7RY0p1ej6D4CKFE6lymSDJpW0YHX/wqE9+cfEauh7xZcG0q9t2ta6F6fmX0agvpFyZo8aFbXeUBr7osSCJNgvavWbM/06niWrOvYX2xwWdhXmXSrbX8ZbabVohBK41 email@example.com"
}


resource "aws_internet_gateway" "tfm-gateway" {
    vpc_id = aws_vpc.tfm-vpc.id
}

resource "aws_default_route_table" "tfm-route-table" {
  default_route_table_id = aws_vpc.tfm-vpc.default_route_table_id


  route  {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.tfm-gateway.id
  }


  tags = {
    Name = "tfm routing table"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.tfm-public.id
  route_table_id = aws_default_route_table.tfm-route-table.id
}

resource "aws_subnet" "tfm-public" {
  vpc_id     = aws_vpc.tfm-vpc.id
  cidr_block = "172.2.0.0/24"
  availability_zone = "us-east-2b"
  tags = {
    Name = "terraform public subnet"
  }
}

resource "aws_security_group" "tfm-sg" {
  name        = "tfm security group"
  description = "Allow tfm inbound traffic"
  vpc_id      = aws_vpc.tfm-vpc.id
  depends_on = [
    aws_vpc.tfm-vpc
  ]

  ingress = [
    {
      description      = "http"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids : null,
      security_groups : null,
      self : null,


    },
    {
      description      = "container"
      from_port        = 5000
      to_port          = 5000
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids : null,
      security_groups : null,
      self : null,


    }
  ]

  egress = [
    {
      description      = "all traffic allowed"
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
      prefix_list_ids : null,
      security_groups : null,
      self : null,

    }
  ]

  tags = {
    Name = "allow_tfm_traffic"
  }
}

resource "aws_eip" "lb" {
  vpc = true
}


resource "aws_lb" "tfm-lb" {
  name               = "test-lb-tf"
  internal           = false
  load_balancer_type = "network"
  depends_on = [
    aws_subnet.tfm-public,
    aws_eip.lb
  ]
  enable_deletion_protection = false

  subnet_mapping {
    subnet_id     = aws_subnet.tfm-public.id
    allocation_id = aws_eip.lb.id
  }

  tags = {
    Environment = "development"
  }
}



resource "aws_ecs_cluster" "tfm-cluster" {
    name = "tfm-cluster"
}

resource "aws_autoscaling_group" "tfm-ecs-cluster" {
    name = "Tfm cluster"
    min_size = "0"
    max_size = "1"
    desired_capacity = "1"
    health_check_type = "EC2"
    launch_configuration = aws_launch_configuration.ecs.name
    vpc_zone_identifier = [aws_subnet.tfm-public.id]
}

resource "aws_launch_configuration" "ecs" {
    name = "ECS ${var.ecs_cluster_name}"
    image_id = lookup(var.amis, var.region)
    instance_type = var.instance_type
    security_groups = [aws_security_group.tfm-sg.id]
    iam_instance_profile = aws_iam_instance_profile.ecs.name
    key_name = aws_key_pair.tfm-key.key_name
    associate_public_ip_address = true
    user_data = "#!/bin/bash\necho ECS_CLUSTER='${var.ecs_cluster_name}' > /etc/ecs/ecs.config"
}

resource "aws_iam_role" "ecs_host_role" {
    name = "ecs_host_role"
    assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs_instance_role_policy" {
    name = "ecs_instance_role_policy"
    policy = file("policies/ecs-instance-role-policy.json")
    role = aws_iam_role.ecs_host_role.id
}

resource "aws_iam_role" "ecs_service_role" {
    name = "ecs_service_role"
    assume_role_policy = file("policies/ecs-role.json")
}

resource "aws_iam_role_policy" "ecs_service_role_policy" {
    name = "ecs_service_role_policy"
    policy = file("policies/ecs-service-role-policy.json")
    role =  aws_iam_role.ecs_service_role.id
}

resource "aws_iam_instance_profile" "ecs" {
    name = "ecs-instance-profile"
    path = "/"
    role = aws_iam_role.ecs_host_role.name
}